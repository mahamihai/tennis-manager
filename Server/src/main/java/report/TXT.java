package report;

import dao.GameProvider;
import dao.IGameProvider;
import dao.IPlayerProvider;
import dao.PlayerProvider;
import model.Game;
import model.Player;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TXT implements Report{

    @Override
    public void generateReport() {


        File file = new File( "Raport.txt");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

        FileWriter fw;
        BufferedWriter bw = null;
        try {
            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String text="Games \r\n\r\n";
        IGameProvider gameProvider=new GameProvider();

        for(Game games: gameProvider.viewGames()){

            text = text + games.toString()+"\r\n\r\n";}
        try {
            bw.write(text);
            bw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
