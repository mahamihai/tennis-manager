package server;

import dao.*;
import model.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.List;

public class CommandInterpreter {

    private IClientProvider clientProvider=new ClientProvider();
    private IChampionshipProvider championshipProvider=new ChampionshipProvider();
    private IPlayerProvider playerProvider=new PlayerProvider();
    private IGameProvider gameProvider=new GameProvider();
    private IRatingProvider ratingProvider=new RatingProvider();
    private boolean notifyObservers=false;
    private Game progressGame=null;
    private IViewProvider viewProvider=new ViewProvider();
    private Command generateCommand(String type, Object data) throws UnsupportedEncodingException {
        Command newCommand=new Command();
        newCommand.setTaskName(type);
        byte[] serialData=SerializableHandler.serialize(data);
        newCommand.setData(serialData);
       return newCommand;
    }

    public void checkForObserving(Game income)
    {
        if(income.getStatus().equals("In progress"))
        {
            notifyObservers=true;
            this.progressGame=income;
        }
    }

    public boolean needToNotify()
    {
        boolean aux=notifyObservers;
        notifyObservers=false;
        return aux;
    }
    public Command getNotifyCommand()
    {
        Command newComm=new Command();
        newComm.setTaskName("Notified");
        try {
            byte[] serializedGame=SerializableHandler.serialize(this.progressGame);
            newComm.setData(serializedGame);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return newComm;

    }
    public Command execute(Command comm) throws IOException, ClassNotFoundException {




        String task=comm.getTaskName();
        Command response=null;
        switch(task)
        {
            case "Login":
                    Client client= SerializableHandler.deserialize(comm.getData(),Client.class);
                    Client found=clientProvider.searchForUser(client.getUsername(),client.getPassword());
                    response= this.generateCommand("Response",found);
                    break;
            case "SearchPlayerByName":
                Player player= SerializableHandler.deserialize(comm.getData(),Player.class);
                List<Player> players=this.playerProvider.searchPlayer(player.getPlayerName());
                response= this.generateCommand("Response",players);
                break;
            case "ViewAllPlayers":
                List<Player> allPlayers=this.playerProvider.viewPlayers();
                response= this.generateCommand("Response",allPlayers);
                break;
            case "ViewAllChampionships":
                List<Championship> championships=this.championshipProvider.viewChampionships();
                response= this.generateCommand("Response",championships);
                break;
            case "ViewAllGames":

                List<Game> gamesFound=this.gameProvider.viewGames();
                response= this.generateCommand("Response",gamesFound);
                break;
            case "SearchGamesByDispute":
                Game game= SerializableHandler.deserialize(comm.getData(),Game.class);
                List<Game> games=this.gameProvider.searchbyDispute(game.getDispute());
                response= this.generateCommand("Response",games);
                break;
            case "SearchChampionshipsByName":
                Championship championship= SerializableHandler.deserialize(comm.getData(),Championship.class);
                List<Championship> championshipsFound=this.championshipProvider.searchChampionshipByName(championship.getChampionshipName());
                response= this.generateCommand("Response",championshipsFound);
                break;
            case "AddPlayer":
                Player newPlayer= SerializableHandler.deserialize(comm.getData(),Player.class);
                this.playerProvider.addPlayer(newPlayer);
                break;
            case "UpdatePlayer":
                Player updatedPlayer= SerializableHandler.deserialize(comm.getData(),Player.class);
                this.playerProvider.updatePlayer(updatedPlayer);
                break;
            case "DeletePlayer":
                Player deletedPlayer= SerializableHandler.deserialize(comm.getData(),Player.class);
                this.playerProvider.deletePlayer(deletedPlayer.getId());
                break;
            case "AddChampionship":
                Championship newChampionship= SerializableHandler.deserialize(comm.getData(),Championship.class);
                this.championshipProvider.addChampionship(newChampionship);
                break;
            case "UpdateChampionship":
                Championship updatedChampionship= SerializableHandler.deserialize(comm.getData(),Championship.class);
                this.championshipProvider.updateChampionship(updatedChampionship);
                break;
            case "DeleteChampionship":
                Championship deletedChampionship= SerializableHandler.deserialize(comm.getData(),Championship.class);
                this.championshipProvider.deleteChampionship(deletedChampionship.getId());
                break;

            case "AddGame":
                Game newGame= SerializableHandler.deserialize(comm.getData(),Game.class);
                checkForObserving(newGame);
                this.gameProvider.addGame(newGame);
                break;
            case "UpdateGame":
                Game updatedGame= SerializableHandler.deserialize(comm.getData(),Game.class);
                this.gameProvider.updateGame(updatedGame);
                break;
            case "DeleteGame":
                Game deletedGame= SerializableHandler.deserialize(comm.getData(),Game.class);
                this.gameProvider.deleteGame(deletedGame.getId());
                break;
            case "ViewAllClients":

                List<Client> clientsFound=this.clientProvider.viewClients();
                response= this.generateCommand("Response",clientsFound);
                break;
            case "AddClient":
                Client newClient= SerializableHandler.deserialize(comm.getData(),Client.class);
                this.clientProvider.addClient(newClient);
                break;
            case "UpdateClient":
                Client updatedClient= SerializableHandler.deserialize(comm.getData(),Client.class);
                this.clientProvider.updateClient(updatedClient);
                break;
            case "DeleteClient":
                Client deletedClient= SerializableHandler.deserialize(comm.getData(),Client.class);
                this.clientProvider.deleteClient(deletedClient.getId());
                break;
            case "AddRating":
                Rating rating=SerializableHandler.deserialize(comm.getData(), Rating.class);
                this.ratingProvider.addRating(rating);
                break;
            case "ViewAllRatings":

                List<Rating> ratingsFound=this.ratingProvider.viewRatings();
                response= this.generateCommand("Response",ratingsFound);
                break;
            case "UpdateRating":
                Rating updatedRating= SerializableHandler.deserialize(comm.getData(),Rating.class);
                this.ratingProvider.updateRating(updatedRating);
                break;
            case "DeleteRating":
                Rating deletedRating= SerializableHandler.deserialize(comm.getData(),Rating.class);
                this.ratingProvider.deleteRating(deletedRating.getId());
                break;
            case "AddView":
                Client viewer=SerializableHandler.deserialize(comm.getData(), Client.class);
                this.viewProvider.addView(viewer.getId());
                break;
            case "NrOfViews":
                Client viewer1=SerializableHandler.deserialize(comm.getData(), Client.class);
                List<View> views=this.viewProvider.getUserViews(viewer1.getId());
                response= this.generateCommand("Response",views);
                break;
        }
        return response;
    }
}
