package server;

import com.sun.xml.internal.xsom.impl.scd.Iterators;
import dao.SerializableHandler;
import model.Client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steven Ou
 */
public class ServerHandler {
    private CommandInterpreter interpreter;

    private AsynchronousServerSocketChannel serverSock;
    private ArrayList<AsynchronousSocketChannel> connectedClients;
    public Map<AsynchronousSocketChannel, Client> connectedClientsAccounts;
    public ServerHandler(String bindAddr, int bindPort ) throws IOException {
        connectedClientsAccounts=new HashMap<AsynchronousSocketChannel, Client>();
        connectedClients=new ArrayList<>();
        this.interpreter=new CommandInterpreter();
        InetSocketAddress sockAddr = new InetSocketAddress(bindAddr, bindPort);

        //create a socket channel and bind to local bind address
        AsynchronousServerSocketChannel serverSock =  AsynchronousServerSocketChannel.open().bind(sockAddr);

        //start to accept the connection from client
        serverSock.accept(serverSock, new CompletionHandler<AsynchronousSocketChannel,AsynchronousServerSocketChannel >() {

            @Override
            public void completed(AsynchronousSocketChannel sockChannel, AsynchronousServerSocketChannel serverSock ) {
               connectedClients.add(sockChannel);
                //a connection is accepted, start to accept next connection
                serverSock.accept( serverSock, this );

                //start to read message from the client
                startRead( sockChannel );

            }

            @Override
            public void failed(Throwable exc, AsynchronousServerSocketChannel serverSock) {
                System.out.println( "fail to accept a connection");
            }

        } );

    }
    public void broadcastAllClients(byte[] message)
    {
        for(AsynchronousSocketChannel aux:this.connectedClients)
        {
            if(aux.isOpen())
            {
                startWrite(aux,message);
            }
        }
    }
    private void startRead( AsynchronousSocketChannel sockChannel ) {
        final ByteBuffer buf = ByteBuffer.allocate(15048);

        //read message from client

        sockChannel.read( buf, sockChannel, new CompletionHandler<Integer, AsynchronousSocketChannel >() {

            /**
             * some message is read from client, this callback will be called
             */
            @Override
            public void completed(Integer result, AsynchronousSocketChannel channel  ) {
                buf.flip();

                String v=new String( buf.array());
                v=v.replace("\u0000","");

                // echo the message
                if(result.intValue()>0) {
                    try {
                        System.out.println("Received+" + result.toString());
                        Command received = SerializableHandler.deserialize(v.getBytes(),Command.class);
                        Command response=interpreter.execute(received);

                        if(response!=null)
                        {
                            byte[] serial=SerializableHandler.serialize(response);
                            startWrite(channel,serial);
                        }
                        if(interpreter.needToNotify())
                        {
                            Command newComm=interpreter.getNotifyCommand();
                           byte[] notifier= SerializableHandler.serialize(newComm);
                            broadcastAllClients(notifier);
                        }
                        System.out.println(received);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                    //start to read next message again
                    startRead(channel);

            }

            @Override
            public void failed(Throwable exc, AsynchronousSocketChannel channel ) {
                System.out.println( "fail to read message from client");
            }
        });
    }

    public void startWrite( AsynchronousSocketChannel sockChannel, byte[] message) {
        ByteBuffer buf = ByteBuffer.allocate(15048);
        buf.put(message);
        buf.flip();
        sockChannel.write(buf, sockChannel, new CompletionHandler<Integer, AsynchronousSocketChannel >() {

            @Override
            public void completed(Integer result, AsynchronousSocketChannel channel) {
                //finish to write message to client, nothing to do
            }

            @Override
            public void failed(Throwable exc, AsynchronousSocketChannel channel) {
                //fail to write message to client
                System.out.println( "Fail to write message to client");
            }

        });
    }


}