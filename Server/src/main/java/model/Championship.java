package model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "Championship")

public class Championship implements Serializable {

    @Id
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    @Column(name = "id")
    private int id;

    public int getId() {
        return this.id;
    }

    @Column(name ="championshipName")
    private String championshipName;

    public String getChampionshipName() {
        return this.championshipName;
    }

    public void setChampionshipName(String championshipName) {
        this.championshipName=championshipName;
    }

    @Column (name = "surface")
    private String surface;

    public String getSurface() { return this.surface; }

    public void setSurface(String surface) { this.surface = surface; }

    @Column (name = "points")
    private int points;

    public int getPoints() { return this.points; }

    public void setPoints(int points) { this.points = points; }

    public String toString(){
        return "A new championship was introduced.\n The new data is: \n" +
                "Championship name: "+championshipName+"\n"+
                "Surface: "+surface+"\n"+
                "Number of winning points: "+points+"\n"+"\n";
    }
}
