package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "Player")
public class Player implements Serializable {

    @Id
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    @Column(name = "id")
    private int id;

    public int getId() {
        return this.id;
    }

    @Column(name ="fullname")
    private String fullname;


    public String getPlayerName() {
        return this.fullname;
    }

    public void setPlayerName(String fullname) {
        this.fullname=fullname;
    }

    @Column(name = "wins")
    private int wins;

    public int getWins() {
        return this.wins;
    }

    public void setWins(int wins){
        this.wins=wins;
    }

    @Column (name = "loses")
    private int loses;

    public int getLoses(){
        return this.loses;
    }

    public void setLoses(int loses) {
        this.loses=loses;
    }

    @Column (name = "bestChamionship")
    private String bestChampionship;

    public String getBestChampionship() {
        return  this.bestChampionship;
    }

    public void setBestChampionship(String bestChampionship) {
        this.bestChampionship = bestChampionship;
    }

    @Column (name = "ranking")
    private int ranking;

    public int getRanking() { return this.ranking; }
    public void setRanking(int ranking) { this.ranking = ranking; }

    public Player (int id, String fullname, int wins, int loses, String bestChampionship, int ranking) {

        this.id = id;
        this.fullname = fullname;
        this.wins = wins;
        this.loses = loses;
        this.bestChampionship = bestChampionship;
        this.ranking = ranking;
    }

    public Player () {

    }

    @Override
    public String toString(){
        return  "A new player was introduced.\n The new data is: \n" +
                "Player name: "+fullname+"\n"+
                "Wins: "+wins+"\n"+
                "Loses: "+loses+"\n"+
                "Best Championship: "+bestChampionship+"\n"+
                "Ranking: "+ranking+"\n"+"\n";
    }

}
