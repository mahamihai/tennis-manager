package dao;

import model.View;

import java.util.List;

public interface IViewProvider {
    void addView(int userId);

    List<View> getUserViews(int userId);
}
