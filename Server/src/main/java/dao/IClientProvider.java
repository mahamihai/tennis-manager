package dao;

import model.Client;

import java.util.List;

public interface IClientProvider {

    void addClient(Client client);
    List<Client> viewClients();
    void deleteClient(int id);
    Client searchbyUsername(String username);
    Client searchbyPassword(String password);

    void updateClient(Client updatedClient);

    Client searchForUser(String username, String password);

}
