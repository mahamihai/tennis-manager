package dao;

import model.Player;

import java.util.List;

public interface IPlayerProvider {

        void addPlayer(Player player);
        List<Player> viewPlayers();
         void updatePlayer(Player updatedPlayer) ;
        void deletePlayer(int id);
        String searchPlayerByName(String fullname);
        List<Player> searchPlayer(String fullname);


}
