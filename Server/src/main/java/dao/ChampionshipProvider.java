package dao;

import model.Championship;
import model.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.util.List;

public class ChampionshipProvider implements IChampionshipProvider {

    private SessionFactory sessionFactory;

    public ChampionshipProvider() {

    }

    @Override
    public void addChampionship(Championship championship) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            //TODO: Don't forget beginTransaction/commit when doing *changes* on the data
            session.beginTransaction();
            session.save(championship);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Championship> viewChampionships() {
        List<Championship> championships = null;
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            championships = session.createQuery("from Championship", Championship.class).list();

        }
        return championships;
    }

    @Override
    public void deleteChampionship(int id) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Championship championship= session.createQuery("from Championship where id=:idd ", Championship.class).setParameter("idd",id).getSingleResult();
            session.delete(championship);
            session.getTransaction().commit();
        }

    }


    @Override
    public void updateChampionship (Championship updateChampionship) {

        sessionFactory= SessionF.getSessionFactory();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Championship championship = session.createQuery("from Championship where id=:idd ", Championship.class).setParameter("idd",updateChampionship.getId()).getSingleResult();
            championship.setChampionshipName(updateChampionship.getChampionshipName());
            championship.setSurface(updateChampionship.getSurface());
            championship.setPoints(updateChampionship.getPoints());
            session.update(championship);
            session.getTransaction().commit();
        }
    }


    @Override
    public String searchChampionship(String championshipName) {

        sessionFactory= SessionF.getSessionFactory();

        Championship championship = new Championship();
        try (Session session = sessionFactory.openSession()) {

            championship = session.createQuery("from Championship where championshipName=:championshipName", Championship.class).setParameter("championshipName",championshipName).getSingleResult();
        }

        return championshipName;

    }

    @Override
    public List<Championship> searchChampionshipByName(String championshipName) {

        sessionFactory= SessionF.getSessionFactory();

        List<Championship> championships = null;
        try (Session session = sessionFactory.openSession()) {

            championships = session.createQuery("from Championship where championshipName=:championshipName", Championship.class).setParameter("championshipName",championshipName).list();

        }
        return championships;
    }


}
