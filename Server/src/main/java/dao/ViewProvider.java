package dao;

import model.Rating;
import model.View;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.util.List;

public class ViewProvider implements IViewProvider {
    private SessionFactory sessionFactory;
    @Override
    public void addView(int userId)
    {
        View view=new View();
        view.setUserId(userId);
        sessionFactory= SessionF.getSessionFactory();

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(view);

                session.getTransaction().commit();
            }
            catch(Exception e)
            {
                System.out.println(e.toString());
        }

    }
    @Override
    public List<View> getUserViews(int userId)
    {
        List<View> views = null;
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            views = session.createQuery("from View where userId=:userId", View.class).setParameter("userId",userId).list();


        }
        return views;
    }
}
