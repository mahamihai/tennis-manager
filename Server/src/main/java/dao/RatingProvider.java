package dao;

import model.Player;
import model.Rating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class RatingProvider implements IRatingProvider {
    private SessionFactory sessionFactory;
    private IGameProvider gameProvider=new GameProvider();
    private List<Rating> getRatingsByGameId(int gameId)
    {
        sessionFactory= SessionF.getSessionFactory();
List<Rating > ratings=null;

        try (Session session = sessionFactory.openSession()) {

            ratings = session.createQuery("from Rating p where p.gameId=:gameId", Rating.class).setParameter("gameId",gameId).list();
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }

        return ratings;

    }
    private void refreshRating(int gameId)
    {

        List<Rating> ratings=this.getRatingsByGameId(gameId);
        double meanRating=0;
        for(Rating aux:ratings)
        {
            meanRating+=aux.getRating()/ratings.size();
        }
       this.gameProvider.refreshRating(gameId,meanRating);

    }

    @Override
    public void addRating(Rating rating) {
        sessionFactory= SessionF.getSessionFactory();
        Rating previous=this.searchForPreviousRating(rating.getUserId(),rating.getGameId());
        if(previous==null) {
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(rating);

                session.getTransaction().commit();
            }
        }
        else
        {
            previous.setRating(rating.getRating());
            this.updateRating(previous);

        }
        this.refreshRating(rating.getGameId());

    }
    @Override
    public void updateRating(Rating rating) {
        sessionFactory= SessionF.getSessionFactory();
        int gameId=0;
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Rating foundRating = session.createQuery("from Rating where id=:idd ", Rating.class).setParameter("idd",rating.getId()).getSingleResult();
            foundRating.setRating(rating.getRating());
            session.update(foundRating);
            gameId=foundRating.getGameId();
            session.getTransaction().commit();
        }
        this.refreshRating(gameId);
    }

    @Override
    public void deleteRating(int id) {
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Rating rating= session.createQuery("from Rating where id=:idd ", Rating.class).setParameter("idd",id).getSingleResult();
            session.delete(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Rating> viewRatings() {
        List<Rating> ratings = null;
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            ratings = session.createQuery("from Rating", Rating.class).list();

            for(Rating rating : ratings) {

                rating.toString();
            }
        }
        return ratings;
    }


    @Override
    public Rating searchForPreviousRating(int userId, int gameId) {

        sessionFactory= SessionF.getSessionFactory();

        Rating rating = null;
        try (Session session = sessionFactory.openSession()) {

            rating = session.createQuery("from Rating p where p.userId=:userId and p.gameId=:gameId" , Rating.class).setParameter("gameId",gameId).setParameter("userId",userId).getSingleResult();
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }

        return rating;

    }
}
