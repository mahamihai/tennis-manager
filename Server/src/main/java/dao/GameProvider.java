package dao;

import model.Client;
import model.Game;
import model.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.util.Date;
import java.util.List;

public class GameProvider implements IGameProvider {

    private SessionFactory sessionFactory;

    public GameProvider() {

    }

    @Override
    public void addGame(Game game) {

        sessionFactory= SessionF.getSessionFactory();
        game.setRating(0);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(game);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Game> viewGames() {
        List<Game> games = null;
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            games = session.createQuery("from Game", Game.class).list();

        }
        return games;
    }

    @Override
    public void deleteGame(int id) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Game game = session.createQuery("from Game where id=:idd ", Game.class).setParameter("idd",id).getSingleResult();
            session.delete(game);
            session.getTransaction().commit();
        }

    }


    @Override
    public void updateGame(Game updatedGame) {

        sessionFactory= SessionF.getSessionFactory();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Game game = session.createQuery("from Game where id=:idd ", Game.class).setParameter("idd",updatedGame.getId()).getSingleResult();
            game.setDispute(updatedGame.getDispute());
            game.setChampionship(updatedGame.getChampionship());
            game.setTime(updatedGame.getTime());
            game.setStatus(updatedGame.getStatus());
            game.setRating(updatedGame.getRating());
            session.update(game);
            session.getTransaction().commit();
        }
    }

    @Override
    public String searchDispute(String dispute) {

        sessionFactory= SessionF.getSessionFactory();

        Game game = new Game();
        try (Session session = sessionFactory.openSession()) {

            game = session.createQuery("from Game where dispute=:dispute", Game.class).setParameter("dispute",dispute).getSingleResult();
        }

        return dispute;

    }
    @Override
    public Game getDisputeById(int gameId)
    {
        Game game=null;
        sessionFactory= SessionF.getSessionFactory();

        try (Session session = sessionFactory.openSession()) {

            game = session.createQuery("from Game where id=:id", Game.class).setParameter("id",gameId).getSingleResult();

        }
        catch(Exception e)
        {
            System.out.println("Probably empty "+e.toString());
        }
        return game;

    }
    @Override
    public void refreshRating(int gameId, double rating) {
        Game found = this.getDisputeById(gameId);
        if (found != null)
        {
            found.setRating(rating);
            this.updateGame(found);
        }
    }


    @Override
    public List<Game> searchbyDispute(String dispute) {
        sessionFactory= SessionF.getSessionFactory();

        List<Game> games = null;
        try (Session session = sessionFactory.openSession()) {

            games = session.createQuery("from Game where dispute=:dispute", Game.class).setParameter("dispute",dispute).list();

        }
        catch(Exception e)
        {
            System.out.println("Probably empty "+e.toString());
        }

        return games;
    }

}
