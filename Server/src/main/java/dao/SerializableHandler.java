package dao;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.sqlite.SQLiteConfig;
import server.Command;
import server.ServerHandler;

import java.io.*;

public  class SerializableHandler {

    public static byte[] serialize (   Object obj) throws UnsupportedEncodingException {
        if(obj==null)
        {
            return null;
        }
        Gson gson = new GsonBuilder()
                // .setPrettyPrinting()
                .create();

       String jsonInString = gson.toJson(obj);;

        return jsonInString.getBytes("UTF-8");

    }

    public static <T> T deserialize(byte[] data,Class<T> type) throws IOException, ClassNotFoundException {

        if(data==null)
        {
            return null;
        }

        Gson gson = new GsonBuilder().create();
        String json=new String(data, "UTF-8");
        T obj = gson.fromJson(json, type);
        return obj;
    }
}
