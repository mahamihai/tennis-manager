package dao;

import model.Client;
import model.Game;

import java.util.Date;
import java.util.List;

public interface IGameProvider {

    void addGame(Game game);
    List<Game> viewGames();
    void deleteGame(int id);
    List<Game> searchbyDispute(String dispute);


    void updateGame(Game updatedGame);

    String searchDispute(String dispute);

    Game getDisputeById(int gameId);

    void refreshRating(int gameId, double rating);

}
