package dao;

import model.Championship;
import model.Client;
import model.Game;
import model.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.util.List;

public class ClientProvider implements IClientProvider {

    private SessionFactory sessionFactory;

    public ClientProvider() {

    }

    @Override
    public void addClient(Client client) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(client);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Client> viewClients() {

        sessionFactory= SessionF.getSessionFactory();
        List<Client> clients = null;
        try (Session session = sessionFactory.openSession()) {
            clients = session.createQuery("from Client", Client.class).list();

        }
        return clients;
    }

    @Override
    public void deleteClient(int id) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Client client= session.createQuery("from Client where id=:idd ", Client.class).setParameter("idd",id).getSingleResult();
            session.delete(client);
            session.getTransaction().commit();
        }

    }


    @Override
    public void updateClient(Client updatedClient) {


        sessionFactory= SessionF.getSessionFactory();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Client client = session.createQuery("from Client where id=:idd ", Client.class).setParameter("idd",updatedClient.getId()).getSingleResult();
            client.setFullname(updatedClient.getFullname());
            client.setUsername(updatedClient.getUsername());
            client.setPassword(updatedClient.getPassword());
            client.setAge(updatedClient.getAge());
            client.setViewsNr(updatedClient.getViewsNr());
            session.update(client);
            session.getTransaction().commit();
        }
    }

@Override
    public Client searchForUser(String username, String password)
    {
        sessionFactory= SessionF.getSessionFactory();

Client client=null;
        try (Session session = sessionFactory.openSession()) {

            client = session.createQuery("from Client where password=:password and username=:username", Client.class).setParameter("password",password).setParameter("username",username).getSingleResult();

        }
        catch(Exception e)
        {
            return null;
        }
        return client;

    }

    @Override
    public Client searchbyUsername(String username) {

        sessionFactory= SessionF.getSessionFactory();

        Client client = new Client();
        try (Session session = sessionFactory.openSession()) {

            client = session.createQuery("from Client where username=:username", Client.class).setParameter("username",username).getSingleResult();

        }
        return client;
    }

    @Override
    public Client searchbyPassword(String password) {

        sessionFactory= SessionF.getSessionFactory();

        Client client = new Client();
        try (Session session = sessionFactory.openSession()) {

            client = session.createQuery("from Client where password=:password", Client.class).setParameter("password",password).getSingleResult();

        }
        return client;
    }


}


