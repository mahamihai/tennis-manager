package dao;

import model.Rating;

import java.util.List;

public interface IRatingProvider {
     void addRating(Rating rating) ;

    void updateRating(Rating rating);
    void deleteRating(int id);
    List<Rating> viewRatings() ;

    Rating searchForPreviousRating(int userId, int gameId);
}
