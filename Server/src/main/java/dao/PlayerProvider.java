package dao;
import model.Player;
import org.hibernate.Session;
import sessionFactory.SessionF;

import org.hibernate.SessionFactory;

import java.util.List;


public class PlayerProvider implements IPlayerProvider {

    private SessionFactory sessionFactory;

    public PlayerProvider() {

    }

    @Override
    public void addPlayer(Player player) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(player);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Player> viewPlayers() {
        List<Player> players = null;
        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            players = session.createQuery("from Player", Player.class).list();

            for(Player player : players) {

                player.toString();
            }
        }
        return players;
    }

    @Override
    public void deletePlayer(int id) {

        sessionFactory= SessionF.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Player player= session.createQuery("from Player where id=:idd ", Player.class).setParameter("idd",id).getSingleResult();
            session.delete(player);
            session.getTransaction().commit();
        }

    }


    @Override
    public void updatePlayer(Player updatedPlayer) {

        sessionFactory= SessionF.getSessionFactory();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Player player = session.createQuery("from Player where id=:idd ", Player.class).setParameter("idd",updatedPlayer.getId()).getSingleResult();
            player.setPlayerName(updatedPlayer.getPlayerName());
            player.setWins(updatedPlayer.getWins());
            player.setLoses(updatedPlayer.getLoses());
            player.setBestChampionship(updatedPlayer.getBestChampionship());
            player.setRanking(updatedPlayer.getRanking());
            session.update(player);
            session.getTransaction().commit();
        }
    }


    @Override
    public String searchPlayerByName(String fullname) {

        sessionFactory= SessionF.getSessionFactory();

        Player player = new Player();
        try (Session session = sessionFactory.openSession()) {

            player = session.createQuery("from Player p where p.fullname=:fullname", Player.class).setParameter("fullname",fullname).getSingleResult();
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }

        return fullname;

    }

    @Override
    public List<Player> searchPlayer(String fullname) {

        sessionFactory= SessionF.getSessionFactory();
        List<Player> players = null;
        try (Session session = sessionFactory.openSession()) {
            players = session.createQuery("from Player where fullname=:fullname", Player.class).setParameter("fullname", fullname).list();

        }
        return players;
    }




}
