package dao;

import model.Championship;
import model.Client;

import java.util.List;

public interface IChampionshipProvider {

    void addChampionship(Championship championship);
    List<Championship> viewChampionships();
    void deleteChampionship(int id);
    List<Championship> searchChampionshipByName(String championshipName);

    void updateChampionship(Championship updateChampionship);

    String searchChampionship(String championshipName);
}
