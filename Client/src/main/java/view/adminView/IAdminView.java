package view.adminView;

import java.util.Date;

interface IAdminDataProvider {

    //CLIENT
    int getClientId();

    String getClientName();

    String getUsername();

    String getPassword();

    int getAge();

    //PLAYER
    int getPlayerId();

    String getPlayerName();

    int getWins();

    int getLoses();

    String getBestChampionship();

    int getRanking();

    //CHAMPIONSHIP
    int getChampionshipId();

    String getChampionshipName();

    String getSurface();

    int getPoints();

    //GAMES
    int getGameId();
    String getDispute();
    String getDisputeChampionship();
    String getTime();
    String getStatus();


}
    interface IAdminProvider
    {

        void showloginView();
        void frameDispose();
    }

    public interface IAdminView extends IAdminDataProvider , IAdminProvider{
        int getViews();

        int getRatingId();

        double getRatingValue();
    }



