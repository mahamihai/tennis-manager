package view.adminView;

import client.ClientHandler;
import client.ClientHandler;
import controller.AdminController;
import dao.*;
import view.adminView.IAdminView;
import view.loginView.LoginView;
import view.tableView.*;

import javax.persistence.OrderBy;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;

public class AdminView implements IAdminView {


    private JPanel panel1;
    private JTextField losesField;
    private JTextField bestChampionship;
    private JTextField namePlayerField;
    private JTextField idPlayerField;
    private JTextField winsField;
    private JTextField rankingField;
    private JTextField surfaceField;
    private JTextField pointsField;
    private JTextField idGameField;
    private JTextField disputeField;
    private JTextField championshipField;
    private JTextField timeField;
    private JButton insertPlayerButton;
    private JButton updatePlayerButton;
    private JButton deletePlayerButton;
    private JButton viewPlayersButton;
    private JTextField idChampionshipField;
    private JTextField idClientField;
    private JTextField nameClientField;
    private JTextField usernameField;
    private JTextField passwordField;
    private JTextField ageField;
    private JTextField championshipNameField;
    private JButton insertChampionship;
    private JButton updateChampionship;
    private JButton deleteChampionship;
    private JButton viewChampionships;
    private JButton insertGame;
    private JButton updateGame;
    private JButton deleteGame;
    private JButton viewGames;
    private JButton insertClient;
    private JButton viewClients;
    private JButton deleteClient;
    private JButton updateClient;
    private JButton PDFButton;
    private JButton TXTButton;
    private JButton backToLogInButton;
    private JComboBox statusField;
    private JTextField textField1;
    private JTextField ratingId;
    private JComboBox ratingBox;
    private JButton viewRatings;
    private JButton updateRatings;
    private JButton deleteRatings;
    private JTextField viewsField;
    private static JFrame frame;



    public void init() {
        frame = new JFrame("Admin Interface");

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.pack();
        frame.setVisible(true);
        this.ratingBox.addItem(5);
        this.ratingBox.addItem(4);

        this.ratingBox.addItem(3);

        this.ratingBox.addItem(2);

        this.ratingBox.addItem(1);



    }

    public AdminView(  ClientHandler client) {

        IClientProvider clientProvider = new ClientProvider();
        IPlayerProvider playerProvider = new PlayerProvider();
        IChampionshipProvider championshipProvider = new ChampionshipProvider();
        IGameProvider gameProvider = new GameProvider();

        Players players = new Players();
        Clients clients = new Clients();
        Championships championships = new Championships();
        Games games = new Games();
        AdminController adminController=new AdminController(this,clientProvider,playerProvider,championshipProvider,gameProvider,players, championships,games,clients,client);
        statusField.addItem("Planned");
        statusField.addItem("In progress");
        statusField.addItem("Completed");




        insertPlayerButton.addActionListener(e-> adminController.addPlayer());
        insertChampionship.addActionListener(e -> adminController.addChampionship());
        insertGame.addActionListener((e -> adminController.addGame()));
        insertClient.addActionListener(e -> adminController.addClient());

        updatePlayerButton.addActionListener(e -> adminController.updatePlayer());
        updateChampionship.addActionListener(e -> adminController.updateChampionship());
        updateGame.addActionListener(e -> adminController.updateGame());
        updateClient.addActionListener(e -> adminController.updateClient());

        deletePlayerButton.addActionListener(e -> adminController.deletePlayer());
        deleteChampionship.addActionListener(e -> adminController.deletePlayer());
        deleteClient.addActionListener(e-> adminController.deleteClient());
        deleteGame.addActionListener(e-> adminController.deleteGame());

        viewClients.addActionListener(e -> adminController.viewClients());
        viewGames.addActionListener(e-> adminController.viewGames());
        viewPlayersButton.addActionListener(e -> adminController.viewPlayers());
        viewChampionships.addActionListener(e -> adminController.viewChampionships());
        //logOutButton.addActionListener(e -> adminController.logOut());
        PDFButton.addActionListener(e -> adminController.generatePDF());
        TXTButton.addActionListener(e -> adminController.generateTxt());
        backToLogInButton.addActionListener((e) -> {
            showloginView();
        this.frameDispose();
        } );

        viewRatings.addActionListener(e->adminController.viewRatings());

        updateRatings.addActionListener(e->adminController.updateRating());
        deleteRatings.addActionListener(e->adminController.deleteRating());
    }

    @Override
    public void showloginView()

    {
        LoginView start = null;
        try {
            start = new LoginView();
        } catch (IOException e) {
            e.printStackTrace();
        }
        start.init();
    }

    @Override
    public void frameDispose() {
        frame.setVisible(false);
        frame.dispose();
    }

    @Override
    public String getPlayerName() { return namePlayerField.getText(); }
    @Override
    public int getPlayerId()   { return Integer.parseInt(idPlayerField.getText()); }
    @Override
    public String getBestChampionship() { return bestChampionship.getText(); }
    @Override
    public int getWins() { return Integer.parseInt(winsField.getText()); }
    @Override
    public int getLoses() { return Integer.parseInt(losesField.getText());  }
    @Override
    public int getRanking() { return Integer.parseInt(rankingField.getText()); }
    @Override
    public int getChampionshipId() { return Integer.parseInt(idChampionshipField.getText()); }
    @Override
    public String getChampionshipName() { return championshipField.getText();}
    @Override
    public String getSurface() { return surfaceField.getText(); }
    @Override
    public int getPoints() { return Integer.parseInt(pointsField.getText()); }
    @Override
    public int getClientId() { return Integer.parseInt(idClientField.getText());}
    @Override
    public String getClientName() { return nameClientField.getText(); }
    @Override
    public String getUsername() { return usernameField.getText();}
    @Override
    public String getPassword() { return passwordField.getText();}
    @Override
    public int getAge() { return Integer.parseInt(ageField.getText());}
    @Override
    public int getGameId() { return Integer.parseInt(idGameField.getText());}
    @Override
    public String getDispute() { return disputeField.getText();}
    @Override
    public String getDisputeChampionship() { return championshipField.getText();}
    @Override
    public String getTime() { return timeField.getText();}

    @Override
    public String getStatus() {
       return  statusField.getSelectedItem().toString();
    }
    @Override
    public int getViews() {
        return   Integer.parseInt(viewsField.getText());
    }
    @Override
    public int getRatingId(){return Integer.parseInt(ratingId.getText());}
    @Override
    public double getRatingValue(){return Double.parseDouble(ratingBox.getSelectedItem().toString());}
}
