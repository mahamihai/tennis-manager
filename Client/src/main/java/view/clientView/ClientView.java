package view.clientView;

import client.ClientHandler;
import controller.AdminController;
import controller.ClientController;
import dao.*;
import model.Client;
import view.loginView.LoginView;
import view.tableView.Championships;
import view.tableView.Clients;
import view.tableView.Games;
import view.tableView.Players;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientView implements IClientView {

    private JPanel panel1;
    private JButton searchChampionshipButton;
    private JButton searchPlayerButton;
    private JButton searchGameButton;
    private JTextField playerNameField;
    private JTextField rankingField;
    private JTextField bestChampionshipField;
    private JTextField championshipNameField;
    private JTextField surfaceField;
    private JTextField disputeField;
    private JButton backToLogInButton;
    private JButton viewAllPlayersButton;
    private JButton viewAllChampionshipsButton;
    private JButton viewAllDisputesButton;
    private JComboBox ratingField;
    private JButton rateButton;
    private JTextField disputeChampionshipField;
    private JTextField winsField;
    private JTextField losesField;
    public static JFrame frame;


    public void init() {

        frame = new JFrame("Client Interface");

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        this.ratingField.addItem(5);
        this.ratingField.addItem(4);
        this.ratingField.addItem(3);
        this.ratingField.addItem(2);
        this.ratingField.addItem(1);



    }

    public ClientView(Client currentClient, ClientHandler clientHandler) {

        IClientProvider clientProvider = new ClientProvider();
        IPlayerProvider playerProvider = new PlayerProvider();
        IChampionshipProvider championshipProvider = new ChampionshipProvider();
        IGameProvider gameProvider = new GameProvider();
        Players players = new Players();
        Clients clients = new Clients();
        Championships championships = new Championships();
        Games games = new Games();
        ClientController clientController =new ClientController(this,playerProvider,championshipProvider,gameProvider,players,championships,games,clientHandler,currentClient);


        searchPlayerButton.addActionListener(e -> clientController.searchPlayerByName());

        searchChampionshipButton.addActionListener(e -> clientController.searchChampionshipByName());

        searchGameButton.addActionListener(e -> clientController.searchbyDispute());

        viewAllChampionshipsButton.addActionListener(e -> clientController.viewChampionships());

        viewAllPlayersButton.addActionListener( e -> clientController.viewPlayers());

        viewAllDisputesButton.addActionListener(e -> clientController.viewDisputes());

        backToLogInButton.addActionListener((e) -> {
            try {
                showloginView();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            this.frameDispose();
        });


        rateButton.addActionListener(e->clientController.rateGame() );
    }



    @Override
    public void frameDispose() {

        frame.setVisible(false);
        frame.dispose();
    }

    @Override
    public String getPlayerName() { return playerNameField.getText(); }


    @Override
    public String getChampionshipName() { return championshipNameField.getText(); }


    @Override
    public String getDispute() { return disputeField.getText(); }

    @Override
    public int getRating(){return Integer.parseInt(ratingField.getSelectedItem().toString());}

    @Override
    public void showloginView() throws IOException

    {

        LoginView start = new LoginView();
        start.init();
    }


}
