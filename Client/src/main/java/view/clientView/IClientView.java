package view.clientView;

import java.io.IOException;

interface ICLientDataProvider {

    String getPlayerName();

    String getChampionshipName();

    String getDispute();


}

interface ICLientViewProvider {

    void init();
    void showloginView() throws IOException;
    void frameDispose();

}

public interface IClientView extends ICLientDataProvider, ICLientViewProvider {
    int getRating();
}