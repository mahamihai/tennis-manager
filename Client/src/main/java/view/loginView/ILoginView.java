package view.loginView;


import client.ClientHandler;
import model.Client;

interface ILoginDataProvider {

    String getUsername();

    String getPassword();

}

interface IViewProvider {

     void showAdminView(ClientHandler client) ;

    void showClientView(Client currentClient, ClientHandler clientHandler);

    void frameDispose();

    void closeProgram();

    void init();

    String showErrorMessage(String message);
}

public interface ILoginView extends ILoginDataProvider, IViewProvider {


}