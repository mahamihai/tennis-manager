package view.loginView;


import client.ClientHandler;
import client.ClientHandler;
import controller.LoginController;
import dao.ClientProvider;
import dao.IClientProvider;
import model.Client;
import view.adminView.IAdminView;
import view.clientView.ClientView;
import view.adminView.AdminView;

import javax.swing.*;
import java.io.IOException;

public class LoginView implements ILoginView {

    private JPanel panel1;
    private JTextField usernameTextField;
    private JButton logInButton;
    private JButton closeButton;
    private JPasswordField passwordField1;
    private JPasswordField passwordTextField;
    public static JFrame frame;


    public void init() {

        frame = new JFrame("Login Interface");

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public LoginView() throws IOException {


        IClientProvider clientProvider = new ClientProvider();
        LoginController loginController = new LoginController(this,clientProvider);
        logInButton.addActionListener((e) -> {
            loginController.Login();
        });
        closeButton.addActionListener(e -> loginController.closeProgram());

    }


    @Override
    public String getUsername() {
        return usernameTextField.getText();
    }

    @Override
    public String getPassword() {
        return passwordField1.getText();
    }

    @Override
    public void showAdminView(ClientHandler client) {
        AdminView admin = new AdminView(client);
        admin.init();

    }

    @Override
    public void showClientView(Client currentClient, ClientHandler clientHandler) {

        ClientView clientView= new ClientView( currentClient,clientHandler);
        clientView.init();

    }

    @Override
    public void frameDispose() {
        frame.setVisible(false);
        frame.dispose();
    }

    public void closeProgram() {
        System.exit(0);
    }

    @Override
    public String showErrorMessage(String message) {

        JOptionPane.showMessageDialog(null, message);
        return "Wrong Username/Password ";
    }


}
