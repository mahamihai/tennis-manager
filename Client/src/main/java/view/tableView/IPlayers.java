package view.tableView;

import model.Player;

import java.util.List;

public interface IPlayers {

    void PlayersTable(List<Player> players);
}
