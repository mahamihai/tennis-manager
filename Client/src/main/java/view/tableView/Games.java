package view.tableView;

import model.Game;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class Games implements IGames {

    private DefaultTableModel model = new DefaultTableModel();

    public Games() {

        model.addColumn("Game ID");
        model.addColumn("Dispute");
        model.addColumn("Championship");
        model.addColumn("Time");
        model.addColumn("Status");
        model.addColumn("Rating");


    }
    @Override
    public void GamesTable (List<Game> games) {




        model.setRowCount(0);
        JTable tp = new JTable(model);

        for(Game game : games) {

            model.addRow(new Object[]{game.getId(),game.getDispute(),game.getChampionship(),game.getTime(),game.getStatus(),game.getRating()});

            System.out.println(game.toString());
        }

        final JFrame framee = new JFrame("Games");
        framee.setSize(500, 500);

        JPanel p = new JPanel();
        p.setBackground(Color.GRAY);
        p.add(new JScrollPane(tp), BorderLayout.CENTER);
        p.setLayout(new FlowLayout());
        framee.setBackground(Color.gray);
        framee.setContentPane(p);
        framee.setVisible(true);
    }



}
