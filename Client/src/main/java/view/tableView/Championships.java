package view.tableView;

import dao.IChampionshipProvider;
import model.Championship;
import model.Player;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class Championships implements IChampionships {

    private DefaultTableModel model = new DefaultTableModel();

    public Championships() {
        model.addColumn("Championship ID");
        model.addColumn("Championship Name");
        model.addColumn("Surface");
        model.addColumn("Points");

    }
    @Override
    public void ChampionshipsTable (List<Championship> championships) {


        model.setRowCount(0);
        JTable tp = new JTable(model);

        for(Championship championship : championships) {

            model.addRow(new Object[]{championship.getId(),championship.getChampionshipName(),championship.getSurface(),championship.getPoints()});

            System.out.println(championship.toString());
        }

        final JFrame framee = new JFrame("Championships");
        framee.setSize(500, 500);
        JPanel p = new JPanel();
        p.setBackground(Color.GRAY);
        p.add(new JScrollPane(tp), BorderLayout.CENTER);
        p.setLayout(new FlowLayout());
        framee.setBackground(Color.gray);
        framee.setContentPane(p);
        framee.setVisible(true);
    }

}
