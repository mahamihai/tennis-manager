package view.tableView;

import model.Player;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class Players implements IPlayers {

    private DefaultTableModel model = new DefaultTableModel();

    public Players() {

        model.addColumn("Player ID");
        model.addColumn("Player Name");
        model.addColumn("Wins");
        model.addColumn("Loses");
        model.addColumn("Best Championship");
        model.addColumn("Ranking");

    }

    @Override
    public void PlayersTable(List<Player> players) {



        model.setRowCount(0);
        JTable tp = new JTable(model);

        for (Player player : players) {

            model.addRow(new Object[]{player.getId(), player.getPlayerName(), player.getWins(), player.getLoses(),
                    player.getBestChampionship(), player.getRanking()});

            System.out.println(player.toString());
        }

        final JFrame framee = new JFrame("Games");
        framee.setSize(500, 500);

        JPanel p = new JPanel();
        p.setBackground(Color.GRAY);
        p.add(new JScrollPane(tp), BorderLayout.CENTER);
        p.setLayout(new FlowLayout());
        framee.setBackground(Color.gray);
        framee.setContentPane(p);
        framee.setVisible(true);

    }
}
