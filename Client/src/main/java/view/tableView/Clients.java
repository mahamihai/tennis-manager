package view.tableView;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class Clients implements IClients {


    private DefaultTableModel model = new DefaultTableModel();

    public Clients() {

        model.addColumn("Client ID");
        model.addColumn("Client Name");
        model.addColumn("username");
        model.addColumn("password");
        model.addColumn("Age");

    }
    @Override
    public void ClientsTable (List<Client> clients) {




        model.setRowCount(0);
        JTable tp = new JTable(model);

        for(Client client : clients) {

            model.addRow(new Object[]{client.getId(),client.getFullname(),client.getUsername(),client.getPassword(),
                    client.getAge()});

            System.out.println(client.toString());
        }

        final JFrame framee = new JFrame("Clients");
        framee.setSize(500, 500);
        JPanel p = new JPanel();
        p.setBackground(Color.GRAY);
        p.add(new JScrollPane(tp), BorderLayout.CENTER);
        p.setLayout(new FlowLayout());
        framee.setBackground(Color.gray);
        framee.setContentPane(p);
        framee.setVisible(true);
    }
}
