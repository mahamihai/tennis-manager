import view.adminView.AdminView;
import view.clientView.ClientView;
import view.loginView.LoginView;

import java.io.IOException;


public class Main {

    public static void main (String[] args) throws IOException {

        //AdminView adminView = new AdminView();
        //adminView.init();
        //ClientView clientView  = new ClientView();
        //clientView.init();

        LoginView loginView = new LoginView();
        loginView.init();
    }
}
