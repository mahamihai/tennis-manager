package report;

import client.ClientHandler;

public interface Report {


    void generateReport(ClientHandler clientHandler);
}
