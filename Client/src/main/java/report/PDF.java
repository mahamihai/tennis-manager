package report;

import client.ClientHandler;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.GameProvider;
import dao.IGameProvider;
import dao.IPlayerProvider;
import dao.PlayerProvider;
import model.Game;
import model.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.print.Book;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PDF implements Report {

    @Override
    public void generateReport(ClientHandler clientHandler) {

        try  {

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("report.pdf"));
            document.open();
            addContent(document,clientHandler);
            document.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }


    }

    public void addContent(Document document,ClientHandler clientHandler) {
        try {


            Anchor anchor = new Anchor("Players");
            anchor.setName("Players");
            Chapter catPart = new Chapter(new Paragraph(anchor), 1);
            Paragraph paragraph = new Paragraph("");
            Section section = catPart.addSection(paragraph);

            IPlayerProvider playerProvider = new PlayerProvider();


            PdfPTable table = new PdfPTable(5);


            PdfPCell cell = new PdfPCell(new Phrase("Name"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Best Championship"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Wins"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Loses"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Ranking"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);


            table.setHeaderRows(1);

            List<Player> players=playerProvider.viewPlayers(clientHandler);
            for (Player player : players) {
                table.addCell(player.getPlayerName());
                table.addCell(player.getBestChampionship());
                table.addCell(Integer.toString(player.getWins()));
                table.addCell(Integer.toString(player.getLoses()));
                table.addCell(Integer.toString(player.getRanking()));

            }

            section.add(table);
            document.add(catPart);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
