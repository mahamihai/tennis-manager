package controller;

import client.ClientHandler;
import client.Command;
import dao.IClientProvider;
import model.Client;
import view.adminView.IAdminView;
import view.loginView.ILoginView;

import java.io.IOException;

public class LoginController {

    public final IClientProvider clientProvider;
    private final ILoginView loginView;
    public LoginController(ILoginView loginView, IClientProvider clientProvider) throws IOException {


        this.clientProvider = clientProvider;
        this.loginView = loginView;

    }

    public void Login() {



       String password=loginView.getPassword();
       String username=loginView.getUsername();
        ClientHandler clientHandler=null;

        try {
            clientHandler=new ClientHandler("localhost",9999);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (loginView.getUsername().equals("admin") && loginView.getPassword().equals("admin")) {
            loginView.showAdminView(clientHandler);
        }
        else {
            try {
                Client currentClient=this.clientProvider.searchForUser(username,password,clientHandler);
                if (currentClient != null) {
                    loginView.showClientView(currentClient,clientHandler);
                    //loginView.init();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }



    }


    public void closeProgram() {

        loginView.closeProgram();
    }
}
