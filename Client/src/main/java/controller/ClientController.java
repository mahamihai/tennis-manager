package controller;

import client.ClientHandler;
import dao.*;
import model.*;
import view.clientView.ClientView;
import view.clientView.IClientView;
import view.tableView.IChampionships;
import view.tableView.IClients;
import view.tableView.IGames;
import view.tableView.IPlayers;

import javax.swing.*;
import java.io.IOException;
import java.util.List;

public class ClientController {

    private final IClientView clientView;
    private final IPlayerProvider playerProvider;
    private final IChampionshipProvider championshipProvider;
    //private final IClientProvider clientProvider;
    private final IGameProvider gameProvider;
    private final IPlayers playersTable;
    private final IChampionships championshipsTable;
    private final IGames gamesTable;
    private final IRatingProvider ratingProvider;
    private final IClientProvider clientProvider;
   // private final IClients clientsTable;
    private Client currentClient;
    private ClientHandler clientHandler;
    public ClientController(ClientView clientView, IPlayerProvider playerProvider, IChampionshipProvider championshipProvider,
                            IGameProvider gameProvider, IPlayers playersTable,
                            IChampionships championshipsTable, IGames gamesTable,ClientHandler clientHandler,
                            Client currentClient) {

        this.clientView = clientView;
        this.playerProvider = playerProvider;
        this.championshipProvider = championshipProvider;
        this.gameProvider = gameProvider;
        this.playersTable = playersTable;
        this.championshipsTable = championshipsTable;
        this.gamesTable = gamesTable;
        this.clientHandler=clientHandler;
        this.currentClient=currentClient;
        this.ratingProvider=new RatingProvider();
        this.clientProvider=new ClientProvider();
       // this.clientsTable = clientsTable;
       // this.clientProvider = clientProvider;
    }

    public void searchPlayerByName() {

        String fullname = clientView.getPlayerName();
        try {

            List<Player> foundPlayers = this.playerProvider.searchPlayer(fullname,clientHandler);
            if (foundPlayers != null) {

                playersTable.PlayersTable(foundPlayers);
            }
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
                }

    }

    public void searchChampionshipByName() {

        String championshipName = clientView.getChampionshipName();
        List<Championship> found=null;
        try {
            found=championshipProvider.searchChampionshipByName(championshipName,clientHandler);
            if (found!=null)
            {

                championshipsTable.ChampionshipsTable(found);
            }
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }


    }
    public void rateGame()
    {
       String disputeName=this.clientView.getDispute();
       List<Game> gamesFound=this.searchbyDisputeName(disputeName);
       if(gamesFound.size()>0)
       {
           Game dispute=gamesFound.get(0);
           Rating newRating=new Rating();
           newRating.setGameId(dispute.getId());


           int rating=this.clientView.getRating();
           newRating.setRating(rating);

           newRating.setUserId(this.currentClient.getId());
            this.ratingProvider.addRating(newRating,clientHandler);

       }

    }
    public List<Game> searchbyDisputeName(String dispute) {
        int nrOfViews=clientProvider.getNrOfViews(this.currentClient,clientHandler);
        if(nrOfViews>=this.currentClient.getViewsNr())
        {
            JOptionPane.showMessageDialog(null, "Exceded number of views");

            return null;

        }
        List<Game> found = null;
        try {
            found = gameProvider.searchbyDispute(dispute, clientHandler);
            this.clientProvider.putView(this.currentClient,this.clientHandler);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
        return found;
    }
        public List<Game> searchbyDispute() {
            int nrOfViews=clientProvider.getNrOfViews(this.currentClient,clientHandler);
            if(nrOfViews>=this.currentClient.getViewsNr())
            {
                JOptionPane.showMessageDialog(null, "Exceded number of views");

                return null;

            }
        List<Game> found=null;
        String dispute = clientView.getDispute();
        try {
            found=gameProvider.searchbyDispute(dispute,clientHandler);
            if (found!=null)
            {

                gamesTable.GamesTable(found);
                this.clientProvider.putView(this.currentClient,this.clientHandler);

            }
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
        return found;

    }


    public void viewPlayers()  {

        try {
            playersTable.PlayersTable(playerProvider.viewPlayers(this.clientHandler));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void viewChampionships() {

        championshipsTable.ChampionshipsTable(championshipProvider.viewChampionships(this.clientHandler));
    }

    public void viewDisputes() {

        gamesTable.GamesTable(gameProvider.viewGames(clientHandler));
    }



}
