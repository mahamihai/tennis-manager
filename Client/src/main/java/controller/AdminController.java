package controller;


import client.ClientHandler;
import dao.*;
import model.*;
import report.PDF;
import report.Report;
import report.TXT;
import view.adminView.IAdminView;
import view.tableView.*;

import javax.swing.*;
import java.io.IOException;
import java.util.List;

public class AdminController {

    private final IAdminView adminView;
    private final IClientProvider clientProvider;
    private final IPlayerProvider playerProvider;
    private final IChampionshipProvider championshipProvider;
    private final IGameProvider gameProvider;
    private final IPlayers playersTable;
    private final IChampionships championshipsTable;
    private final IGames gamesTable;
    private final IClients clientsTable;
    private final ClientHandler clientHandler;
    private final IRating ratingsTable;
private final IRatingProvider ratingProvider;

    public AdminController(IAdminView adminView, IClientProvider clientProvider, IPlayerProvider playerProvider,
                           IChampionshipProvider championshipProvider, IGameProvider gameProvider, IPlayers playersTable,
                           IChampionships championshipsTable, IGames gamesTable, IClients clientsTable, ClientHandler clientHandler)
    {
        this.adminView = adminView;
        this.clientProvider = clientProvider;
        this.playerProvider = playerProvider;
        this.championshipProvider = championshipProvider;
        this.gameProvider = gameProvider;
        this.playersTable = playersTable;
        this.championshipsTable = championshipsTable;
        this.gamesTable = gamesTable;
        this.clientsTable = clientsTable;
        this.clientHandler=clientHandler;
this.ratingProvider=new RatingProvider();
this.ratingsTable=new Ratings();
    }

    public void addPlayer() {

        try  {
            if(!(adminView.getPlayerName().equals("") || adminView.getBestChampionship().equals("") || adminView.getWins()==0
                    || adminView.getLoses()==0 || adminView.getRanking()==0)) {
                Player player = new Player();
                player.setPlayerName(adminView.getPlayerName());
                player.setWins(adminView.getWins());
                player.setLoses(adminView.getLoses());
                player.setBestChampionship(adminView.getBestChampionship());
                player.setRanking(adminView.getRanking());

                playerProvider.addPlayer(player,clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Player data was inserted successfully");
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }

    }

    public void updatePlayer() {
        try {
            if(!(adminView.getPlayerName().equals("") || adminView.getBestChampionship().equals("") || adminView.getWins()==0
                    || adminView.getLoses()==0 || adminView.getRanking()==0)) {

                playerProvider.updatePlayer(adminView.getPlayerId(), adminView.getPlayerName(),adminView.getWins(),
                                            adminView.getLoses(),adminView.getBestChampionship(),adminView.getRanking(),clientHandler);
            }
            JOptionPane.showMessageDialog(null, "Player data was updated successfully");
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }

    public void deletePlayer() {

        try {
            if(!(adminView.getPlayerId()==0)) {

                playerProvider.deletePlayer(adminView.getPlayerId(),clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Player data was deleted successfully");
        }

        catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }

    public void viewPlayers() {

        try {
            playersTable.PlayersTable(playerProvider.viewPlayers(this.clientHandler));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void addChampionship() {

        try  {
            if(!(adminView.getChampionshipName().equals("") || adminView.getSurface().equals("") || adminView.getPoints()==0)) {
                Championship championship = new Championship();
                championship.setChampionshipName(adminView.getChampionshipName());
                championship.setSurface(adminView.getSurface());
                championship.setPoints(adminView.getPoints());

                championshipProvider.addChampionship(championship,clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Championship data was inserted successfully");
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }

    }

    public void updateChampionship() {
        try {
            if(!(adminView.getChampionshipName().equals("") || adminView.getSurface().equals("") || adminView.getPoints()==0)) {

                championshipProvider.updateChampionship(adminView.getChampionshipId(), adminView.getChampionshipName(),adminView.getSurface(),
                        adminView.getPoints(),clientHandler);
            }
            JOptionPane.showMessageDialog(null, "Championship data was updated successfully");
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }

    public void deleteChampionship() {

        try {
            if(!(adminView.getChampionshipId()==0)) {

                championshipProvider.deleteChampionship(adminView.getChampionshipId(),clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Championship data was deleted successfully");
        }

        catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }



    public void viewChampionships() {

        championshipsTable.ChampionshipsTable(championshipProvider.viewChampionships(clientHandler));
    }


    public void addGame() {

        try  {
            if(!(adminView.getDispute().equals("") || adminView.getChampionshipName().equals("") || adminView.getTime().equals(""))) {
                Game game = new Game();
                game.setDispute(adminView.getDispute());
                game.setChampionship(adminView.getChampionshipName());
                game.setTime(adminView.getTime());
                game.setStatus(adminView.getStatus());
                gameProvider.addGame(game,clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Game data was inserted successfully");
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }

    }

    public void updateGame() {
        try {
            if(!(adminView.getDispute().equals("") || adminView.getChampionshipName().equals("") || adminView.getTime().equals(""))) {

                gameProvider.updateGame(adminView.getGameId(),adminView.getDispute(), adminView.getDisputeChampionship(),adminView.getTime(),clientHandler,adminView.getStatus());
            }
            JOptionPane.showMessageDialog(null, "Championship data was updated successfully");
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }



        public void deleteGame() {

            try {
                if(!(adminView.getGameId()==0)) {

                    gameProvider.deleteGame(adminView.getGameId(),clientHandler);
                }

                JOptionPane.showMessageDialog(null, "Game data was deleted successfully");
            }

            catch (Exception ex) {

                JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
            }
        }


    public void viewGames() {

        gamesTable.GamesTable(gameProvider.viewGames(clientHandler));
    }

    public void addClient() {

        try  {
            if(!(adminView.getClientName().equals("") || adminView.getUsername().equals("") || adminView.getPassword().equals("") ||
                    adminView.getAge()==0) ) {
                Client client= new Client();
                client.setFullname(adminView.getClientName());
                client.setUsername(adminView.getUsername());
                client.setPassword(adminView.getPassword());
                client.setAge(adminView.getAge());
                client.setViewsNr(adminView.getViews());

                clientProvider.addClient(client,clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Client data was inserted successfully");
        }

        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }



    }

    public void updateClient() {
        try {
            if(!(adminView.getClientName().equals("") || adminView.getUsername().equals("") || adminView.getPassword().equals("") ||
                    adminView.getAge()==0) || adminView.getViews()>0) {
                String t=adminView.getClientName();
                clientProvider.updateClient(adminView.getClientId(),adminView.getClientName(), adminView.getUsername(),adminView.getPassword(), adminView.getAge(),adminView.getViews(),clientHandler);
            }
            JOptionPane.showMessageDialog(null, "Client data was updated successfully");
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }



    public void deleteClient() {

        try {
            if(!(adminView.getClientId()==0)) {

                clientProvider.deleteClient(adminView.getClientId(),clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Client data was deleted successfully");
        }

        catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }

    public void viewClients() {

        clientsTable.ClientsTable(clientProvider.viewClients(clientHandler));
    }

    public void generatePDF()
    {
        Report report =new PDF();
        report.generateReport(clientHandler);
    }


    public void generateTxt()
    {
        Report report =new TXT();
        report.generateReport(clientHandler);
    }





    public void updateRating() {
        try {
            if(!(adminView.getRatingValue()<1 ||adminView.getRatingValue()>5 || adminView.getRatingId()<0)) {
                Rating newRating=new Rating();
                newRating.setId(adminView.getRatingId());
                newRating.setRating(adminView.getRatingValue());
                this.ratingProvider.updateRating(newRating,clientHandler);
            }
            JOptionPane.showMessageDialog(null, "Rating data was updated successfully");
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }



    public void deleteRating() {

        try {
            if(!(adminView.getRatingId()==0)) {

                ratingProvider.deleteRating(adminView.getRatingId(),clientHandler);
            }

            JOptionPane.showMessageDialog(null, "Rating data was deleted successfully");
        }

        catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "Inserted data is incorrect");
        }
    }

    public void viewRatings() {

        ratingsTable.RatingsTable(ratingProvider.viewRatings(clientHandler));
    }






}

