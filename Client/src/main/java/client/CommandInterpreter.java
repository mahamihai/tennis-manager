package client;

import dao.*;
import model.Championship;
import model.Client;
import model.Game;
import model.Player;

import javax.swing.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class CommandInterpreter {


    public Command execute(Command comm) throws IOException, ClassNotFoundException {




        String task=comm.getTaskName();
        Command response=null;
        switch(task)
        {
            case "Error":
                JOptionPane.showMessageDialog(null, "Not allowed");

                break;
            case "Notified":
                Game progressGame=SerializableHandler.deserialize(comm.getData(),Game.class);
                JOptionPane.showMessageDialog(null, "The game "+progressGame.getDispute() +" has started at "+progressGame.getTime());

                break;

        }
        return response;
    }
}
