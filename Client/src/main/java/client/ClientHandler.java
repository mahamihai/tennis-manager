package client;

import dao.SerializableHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steven
 */
public class ClientHandler {
    private AsynchronousSocketChannel sockChannel;
    private byte[] response=null;
    private CommandInterpreter interpreter=new CommandInterpreter();

    public Command awaitResponse()
    {
        Command comm=null;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            comm= SerializableHandler.deserialize(this.response,Command.class);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        response=null;
        return comm;

    }
    public ClientHandler(String host, int port ) throws IOException {
        //create a socket channel
         this.sockChannel = AsynchronousSocketChannel.open();

        //try to connect to the server side
        sockChannel.connect( new InetSocketAddress(host, port), sockChannel, new CompletionHandler<Void, AsynchronousSocketChannel >() {
            @Override
            public void completed(Void result, AsynchronousSocketChannel channel ) {
                //start to read message
                startRead(  );

                //write an message to server side
                //startWrite( channel );
            }

            @Override
            public void failed(Throwable exc, AsynchronousSocketChannel channel) {
                System.out.println( "fail to connect to server");
            }

        });
    }
public void checkReceived()
{
    try {
        Command received = SerializableHandler.deserialize(this.response,Command.class);
        this.interpreter.execute(received);
    } catch (IOException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }


}
    private void startRead(  ) {
        final ByteBuffer buf = ByteBuffer.allocate(15048);

        sockChannel.read( buf, sockChannel, new CompletionHandler<Integer, AsynchronousSocketChannel>(){

            @Override
            public void completed(Integer result, AsynchronousSocketChannel channel) {
                //message is read from server

                //print the message
                String v=new String( buf.array());
                v=v.replace("\u0000","");

                response=v.getBytes();
                checkReceived();
                startRead();
            }

            @Override
            public void failed(Throwable exc, AsynchronousSocketChannel channel) {
                System.out.println( "fail to read message from server");
            }

        });

    }
    public void startWrite( byte[] message ) {
        ByteBuffer buf = ByteBuffer.allocate(15048);
        buf.put(message);
        buf.flip();
        sockChannel.write(buf, sockChannel, new CompletionHandler<Integer, AsynchronousSocketChannel >() {
            @Override
            public void completed(Integer result, AsynchronousSocketChannel channel ) {
                //after message written
                //NOTHING TO DO
            }

            @Override
            public void failed(Throwable exc, AsynchronousSocketChannel channel) {
                System.out.println( "Fail to write the message to server");
            }
        });
    }



}