package dao;

import client.ClientHandler;
import model.Client;

import java.io.IOException;
import java.util.List;

public interface IClientProvider {


    void addClient(Client client, ClientHandler clientHandler);


    List<Client> viewClients(ClientHandler clientHandler);

    void deleteClient(int id, ClientHandler clientHandler);


    void putView(Client currentClient, ClientHandler clientHandler);

    int getNrOfViews(Client currentClient, ClientHandler clientHandler);

    void updateClient(int id, String fullname, String username, String password, int age, int viewsNr, ClientHandler clientHandler);

    Client searchbyUsername(String username);
    Client searchbyPassword(String password);
     Client searchForUser(String username, String password, ClientHandler clientHandler) throws IOException, ClassNotFoundException;


}
