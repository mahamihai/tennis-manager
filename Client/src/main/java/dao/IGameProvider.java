package dao;

import client.ClientHandler;
import model.Client;
import model.Game;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface IGameProvider {


    void addGame(Game game, ClientHandler clientHandler);

    List<Game> viewGames(ClientHandler clientHandler);


    void deleteGame(int id, ClientHandler clientHandler);

    void updateGame(int id, String dispute, String championship, String time, ClientHandler clientHandler,String status);

    List<Game> searchbyDispute(String dispute, ClientHandler clientHandler) throws IOException, ClassNotFoundException;
}
