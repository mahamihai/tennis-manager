package dao;

import client.ClientHandler;
import client.Command;
import model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import sessionFactory.SessionF;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ClientProvider implements IClientProvider {

    private SessionFactory sessionFactory;

    public ClientProvider() {

    }

    @Override
    public void addClient(Client client, ClientHandler clientHandler) {

        try {
            SerializableHandler.sendCommand("AddClient",client,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Client> viewClients(ClientHandler clientHandler) {
        List<Client> clients = null;

        try {
            SerializableHandler.sendCommand("ViewAllClients",null,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Command response=clientHandler.awaitResponse();
        try {
            clients=SerializableHandler.deserializeList(response.getData(),Client.class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clients;
    }

    @Override
    public void deleteClient(int id, ClientHandler clientHandler) {

        Client deletedClients=new Client();
        deletedClients.setId(id);

        try {
            SerializableHandler.sendCommand("DeleteClient",deletedClients,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
@Override
public void putView(Client currentClient,ClientHandler clientHandler)
{
    try {
        SerializableHandler.sendCommand("AddView",currentClient,clientHandler);
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
}
    @Override
    public int getNrOfViews(Client currentClient, ClientHandler clientHandler)
    {
        try {
            SerializableHandler.sendCommand("NrOfViews",currentClient,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        List<View> views=null;
        Command response=clientHandler.awaitResponse();
        try {
            views=SerializableHandler.deserializeList(response.getData(),View.class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return views.size();
    }
    @Override
    public void updateClient(int id, String fullname, String username, String password, int age,int viewsNr,ClientHandler clientHandler) {

        Client updatedClient=new Client();
        updatedClient.setPassword(password);
        updatedClient.setUsername(username);
        updatedClient.setAge(age);
        updatedClient.setFullname(fullname);
        updatedClient.setId(id);
        updatedClient.setViewsNr(viewsNr);

        try {
            SerializableHandler.sendCommand("UpdateClient",updatedClient,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }




    @Override
    public Client searchbyUsername(String username) {

        sessionFactory= SessionF.getSessionFactory();

        Client client = new Client();
        try (Session session = sessionFactory.openSession()) {

            client = session.createQuery("from Client where username=:username", Client.class).setParameter("username",username).getSingleResult();

        }
        return client;
    }

    public Client searchForUser(String username, String password, ClientHandler clientHandler) throws IOException, ClassNotFoundException {
        Client client = new Client();
        client.setUsername(username);
        client.setPassword(password);

        SerializableHandler.sendCommand("Login",client,clientHandler);
        Command response=clientHandler.awaitResponse();


        Client found=SerializableHandler.deserialize(response.getData(),Client.class);

        return found;
    }
    @Override
    public Client searchbyPassword(String password) {

        sessionFactory= SessionF.getSessionFactory();

        Client client = new Client();
        try (Session session = sessionFactory.openSession()) {

            client = session.createQuery("from Client where password=:password", Client.class).setParameter("password",password).getSingleResult();

        }
        return client;
    }


}


