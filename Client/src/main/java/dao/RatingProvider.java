package dao;

import client.ClientHandler;
import client.Command;
import model.Client;
import model.Player;
import model.Rating;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class RatingProvider implements IRatingProvider {
    @Override
    public void addRating(Rating rating, ClientHandler clientHandler) {
        try {
            SerializableHandler.sendCommand("AddRating",rating,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void updateRating(Rating updatedRating, ClientHandler clientHandler)
    {

        try {
            SerializableHandler.sendCommand("UpdateRating",updatedRating,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

            }
    }
    @Override
    public List<Rating> viewRatings(ClientHandler clientHandler) {
        List<Rating> ratings = null;

        try {
            SerializableHandler.sendCommand("ViewAllRatings",null,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Command response=clientHandler.awaitResponse();
        try {
            ratings=SerializableHandler.deserializeList(response.getData(),Rating.class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return ratings;
    }

    @Override
    public void deleteRating(int id, ClientHandler clientHandler) {
        Rating rating=new Rating();
        rating.setId(id);

        try {
            SerializableHandler.sendCommand("DeleteRating",rating,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




    }
}
