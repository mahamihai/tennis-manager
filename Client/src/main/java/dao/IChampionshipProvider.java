package dao;

import client.ClientHandler;
import model.Championship;

import java.util.List;

public interface IChampionshipProvider {


    void addChampionship(Championship championship, ClientHandler clientHandler);

    List<Championship> viewChampionships(ClientHandler clientHandler) ;



    void deleteChampionship(int id, ClientHandler clientHandler);


    void updateChampionship(int id, String championshipName, String surface, int points, ClientHandler clientHandler);

    List<Championship> searchChampionshipByName(String championshipName, ClientHandler clientHandler);
}
