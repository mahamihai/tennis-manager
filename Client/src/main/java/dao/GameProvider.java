package dao;

import client.ClientHandler;
import client.Command;
import model.Championship;
import model.Client;
import model.Game;
import model.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

public class GameProvider implements IGameProvider {

    private SessionFactory sessionFactory;

    public GameProvider() {

    }

    @Override
    public void addGame(Game game, ClientHandler clientHandler)
    {

        try {
            SerializableHandler.sendCommand("AddGame",game,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Game> viewGames(ClientHandler clientHandler) {
        List<Game> games = null;

        try {
            SerializableHandler.sendCommand("ViewAllGames",null,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Command response=clientHandler.awaitResponse();
        try {
            games=SerializableHandler.deserializeList(response.getData(),Game.class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return games;
    }

    @Override
    public void deleteGame(int id, ClientHandler clientHandler) {

        Game deletedGame=new Game();
        deletedGame.setId(id);

        try {
            SerializableHandler.sendCommand("DeleteGame",deletedGame,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void updateGame(int id, String dispute, String championship, String time, ClientHandler clientHandler,String status) {
        Game updatedGame=new Game();
        updatedGame.setDispute(dispute);
        updatedGame.setChampionship(championship);
        updatedGame.setStatus(status);

        updatedGame.setTime(time);
        updatedGame.setId(id);
        try {
        SerializableHandler.sendCommand("UpdateGame",updatedGame,clientHandler);
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }

}




    @Override
    public List<Game> searchbyDispute(String dispute, ClientHandler clientHandler) throws IOException, ClassNotFoundException {
        Game game = new Game();
        game.setDispute(dispute);



        SerializableHandler.sendCommand("SearchGamesByDispute",game,clientHandler);

        Command response=clientHandler.awaitResponse();


        List<Game> found= null;

        found = SerializableHandler.deserializeList(response.getData(), Game.class);

        return found;
    }

}
