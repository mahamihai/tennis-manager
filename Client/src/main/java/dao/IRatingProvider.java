package dao;

import client.ClientHandler;
import model.Rating;

import java.io.IOException;
import java.util.List;

public interface IRatingProvider {
     void addRating(Rating rating, ClientHandler clientHandler);

    void updateRating(Rating updatedRating, ClientHandler clientHandler);

    List<Rating> viewRatings(ClientHandler clientHandler) ;

    void deleteRating(int id, ClientHandler clientHandler);
}
