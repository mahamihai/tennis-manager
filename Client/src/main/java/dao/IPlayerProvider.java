package dao;

import client.ClientHandler;
import model.Player;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface IPlayerProvider {


    void addPlayer(Player player, ClientHandler clientHandler);

    List<Player> viewPlayers(ClientHandler clientHandler) throws IOException, ClassNotFoundException;

    void deletePlayer(int id, ClientHandler clientHandler);



    void updatePlayer(int id, String fullname, int wins, int loses, String bestChampionship, int ranking, ClientHandler clientHandler);

    List<Player> searchPlayer(String fullname,ClientHandler clientHandler) throws IOException, ClassNotFoundException;


}
