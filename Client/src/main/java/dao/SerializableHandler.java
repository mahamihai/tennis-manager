package dao;

import client.ClientHandler;
import client.Command;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import model.Client;
import org.sqlite.SQLiteConfig;
import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public  class SerializableHandler {

    public static void sendCommand(String type ,Object data, ClientHandler client) throws UnsupportedEncodingException {


        Command comm=new Command();
        comm.setTaskName(type);
        byte[] serialData=serialize(data);
        comm.setData(serialData);
        byte[] serialized=serialize(comm);
        client.startWrite(serialized);
    }
    public static byte[] serialize (   Object obj) throws UnsupportedEncodingException {
        if(obj==null)
        {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();



            // Convert object to JSON string and save into a file directly


            // Convert object to JSON string
        String jsonInString = null;

        jsonInString =new Gson().toJson(obj);;

       return jsonInString.getBytes("UTF-8");

        }

    public static <T> List<T> deserializeList(byte[] data, Class<T> type) throws IOException, ClassNotFoundException {
        if(data==null)
        {
            return null;
        }

        Gson gson = new Gson();
        String json=new String(data, "UTF-8");




        @SuppressWarnings("serial")
        Type collectionType = new TypeToken<List<T>>() {
        }.getType();
        List<T> list = gson.fromJson(json, collectionType);
        List<T> list2=list.stream().map(x->{

            return gson.fromJson(gson.toJson(x),type);
        }).collect(Collectors.toList());
        return list2;
    }

        public static <T> T deserialize(byte[] data,Class<T> type) throws IOException, ClassNotFoundException {

            if(data==null)
            {
                return null;
            }

        Gson gson = new GsonBuilder().create();
        String json=new String(data, "UTF-8");
        T obj = gson.fromJson(json, type);
        return obj;
    }
}
