package dao;
import client.ClientHandler;
import client.Command;
import model.Client;
import model.Player;
import org.hibernate.Session;
import sessionFactory.SessionF;

import org.hibernate.SessionFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class PlayerProvider implements IPlayerProvider {

    private SessionFactory sessionFactory;

    public PlayerProvider() {

    }

    @Override
    public void addPlayer(Player player, ClientHandler clientHandler) {

        try {
            SerializableHandler.sendCommand("AddPlayer",player,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




    }

    @Override
    public List<Player> viewPlayers(ClientHandler clientHandler) throws IOException, ClassNotFoundException {
        List<Player> players = null;

        SerializableHandler.sendCommand("ViewAllPlayers",null,clientHandler);
        Command response=clientHandler.awaitResponse();
        players=SerializableHandler.deserializeList(response.getData(),Player.class);
        return players;
    }

    @Override
    public void deletePlayer(int id, ClientHandler clientHandler) {
        Player player=new Player();
        player.setId(id);

        try {
            SerializableHandler.sendCommand("DeletePlayer",player,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




    }


    @Override
    public void updatePlayer(int id, String fullname, int wins, int loses, String bestChampionship, int ranking,ClientHandler clientHandler) {
        Player updatePlayer=new Player();
        updatePlayer.setPlayerName(fullname);
        updatePlayer.setWins(wins);
        updatePlayer.setLoses(loses);
        updatePlayer.setBestChampionship(bestChampionship);
        updatePlayer.setRanking(ranking);
        updatePlayer.setId(id);
        try {
            SerializableHandler.sendCommand("UpdatePlayer",updatePlayer,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }




    @Override
    public List<Player> searchPlayer(String fullname,ClientHandler clientHandler) throws IOException, ClassNotFoundException {


        Player player = new Player();
        player.setPlayerName(fullname);

        SerializableHandler.sendCommand("SearchPlayerByName",player,clientHandler);
        Command response=clientHandler.awaitResponse();




        List<Player> players = null;
        players=SerializableHandler.deserializeList(response.getData(),Player.class);
        return players;
    }




}
