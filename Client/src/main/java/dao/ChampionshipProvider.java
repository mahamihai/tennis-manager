package dao;

import client.ClientHandler;
import client.Command;
import model.Championship;
import model.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import sessionFactory.SessionF;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ChampionshipProvider implements IChampionshipProvider {

    private SessionFactory sessionFactory;

    public ChampionshipProvider() {

    }

    @Override
    public void addChampionship(Championship championship, ClientHandler clientHandler) {

        try {
            SerializableHandler.sendCommand("AddChampionship",championship,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Championship> viewChampionships(ClientHandler clientHandler) {
        List<Championship> championships = null;



        try {
            SerializableHandler.sendCommand("ViewAllChampionships",null,clientHandler);
            Command response=clientHandler.awaitResponse();
            championships=SerializableHandler.deserializeList(response.getData(),Championship.class);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return championships;
    }

    @Override
    public void deleteChampionship(int id, ClientHandler clientHandler) {

        Championship championship=new Championship();
        championship.setId(id);

        try {
            SerializableHandler.sendCommand("DeleteChampionship",championship,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void updateChampionship (int id, String championshipName, String surface, int points,ClientHandler clientHandler) {

        Championship updateChampionship=new Championship();
        updateChampionship.setChampionshipName(championshipName);
        updateChampionship.setSurface(surface);
        updateChampionship.setPoints(points);
        updateChampionship.setId(id);

        try {
            SerializableHandler.sendCommand("UpdateChampionship",updateChampionship,clientHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }




    @Override
    public List<Championship> searchChampionshipByName(String championshipName, ClientHandler clientHandler) {


        List<Championship> championship = null;
        Championship searchCriteria=new Championship();
        searchCriteria.setChampionshipName(championshipName);

        try {
            SerializableHandler.sendCommand("SearchChampionshipsByName",searchCriteria,clientHandler);
            Command response=clientHandler.awaitResponse();
            championship=SerializableHandler.deserializeList(response.getData(),Championship.class);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return championship;
    }


}
