package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "Client")

public class Client implements Serializable{
    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    @Column(name = "id")
    private int id;

    public int getId() {
        return this.id;
    }

    @Column(name ="fullname")
    private String fullname;

    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname=fullname;
    }

    @Column(name = "username")
    private String username;

    public String getUsername(){
        return this.username;
    }

    public void setUsername(String username){
        this.username=username;
    }

    @Column (name = "password")
    private String password;

    public String getPassword(){
        return  this.password;
    }

    public void setPassword(String password) {
        this.password=password;
    }

    @Column (name = "age")
    private int age;

    public int getAge() { return this.age; }

    public void setAge(int age) { this.age = age; }

    public String toString(){
        return "A new Client was introduced.\n The new data is: \n" +
                "Client name: "+fullname+"\n"+
                "Username: "+username+"\n"+
                "Password: "+password+"\n"+
                "Age: "+age+"\n"+"\n";

    }

    public int getViewsNr() {
        return viewsNr;
    }

    public void setViewsNr(int viewsNr) {
        this.viewsNr = viewsNr;
    }

    @Column (name = "viewsNr")
    private int viewsNr;

}
