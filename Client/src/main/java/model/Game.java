package model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name= "Game")
public class Game implements Serializable{

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    @Column(name = "id")
    private int id;

    public int getId() {
        return this.id;
    }

    @Column(name ="dispute")
    private String dispute;

    public String getDispute() { return this.dispute; }
    public void setDispute(String dispute) { this.dispute = dispute; }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name ="status")
    private String status;
    @Column(name = "championship")
    private String championship;
    public String getChampionship() { return this.championship; }
    public void setChampionship(String championship) { this.championship = championship; }

    @Column(name = "time")
    private String time;
    public String getTime() { return this.time; }
    public void setTime(String time) { this.time = time; }


    public String toString(){
        return  "A new game was introduced.\n " +
                "The new data is:\n" +
                "The game will be disputed between: "+dispute+"\n"+
                "Game takes place at: "+championship+"\n"+
                "Game will start at: "+time+"\n"+"\n";

    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Column(name = "Rating")
    private double rating;


}
